#!/usr/bin/env python

from getpass import getpass
import requests
import argparse
import ConfigParser
import os
import subprocess
import sys
import signal

# don't stacktrace on SIGINT (CTRL+C) - it's ugly.
def sigint_handler(signum, frame):
	print ""
	sys.exit(0)
signal.signal(signal.SIGINT, sigint_handler)

# --------------------------------------------------------------------------
# things that we'll need to populate
# --------------------------------------------------------------------------
cwd = os.path.basename(os.getcwd())
user = None
pw = None
repo = None
clone_link = None
is_private = None

# -------------------------------------------------------------------------
# setup the arg parser early, so that the help runs first
# -------------------------------------------------------------------------
help_msg = """
share a local git repo on bitbucket (create remote and add as origin)

default user and password can be specified in ~/.bbconfig

    example ~/.bbconfig
    -------------------
    [auth]
    user = <user>
    password = <password>

"""
parser = argparse.ArgumentParser(description=help_msg, formatter_class=argparse.RawDescriptionHelpFormatter)
parser.add_argument('-u', '--user', required=False, help="your bitbucket username")
parser.add_argument('-p', '--password', required=False, help="your bitbucket password")
parser.add_argument('--public', required=False, dest='public', action='store_true', help="make this a public repo (defaults to false)")
parser.add_argument('-r', '--repo', required=False, help="the repo name (defaults to current dir name)")

args = parser.parse_args()


# --------------------------------------------------------------------------
# check if we're in a git repo with commits or exit with snarky error
# --------------------------------------------------------------------------
with open(os.devnull, 'w') as f:
	rc = subprocess.call(["git", "branch"], stdout=f, stderr=f)
	if rc != 0:
		print "I'm afraid this dir is not a git repo. Have you tried `git init?`?"
		sys.exit(1)

with open(os.devnull, 'w') as f:
	rc = subprocess.call(['git', 'log'], stdout=f, stderr=f)
	if rc != 0:
		print "Welp, this looks like a git repo but doesn't have any commits.  Commit something first."
		sys.exit(2)

# -------------------------------------------------------------------------
# so we're a git repo, make sure we don't already have an origin 
# -------------------------------------------------------------------------
with open(os.devnull, 'w') as f:
	rc = subprocess.call(['git', 'remote', 'get-url', 'origin'], stdout=f, stderr=f)
	if ( rc == 0 ):
		origin = subprocess.check_output(['git', 'remote', 'get-url', 'origin'], stderr=f)
		print "you seem confused - this repo already has an origin at {0}".format(origin.strip())
		sys.exit(50)

# --------------------------------------------------------------------------
# read the config if it exists
# --------------------------------------------------------------------------
cfg_path = os.path.expanduser('~/.bbshare')
if ( os.path.exists(cfg_path) ):
	config = ConfigParser.ConfigParser()
	config.read(cfg_path)
	# not sure why they make it so hard to check if sections/values exist - so we'll be bad
	# and just catch the exceptions
	try:
		user = config.get('auth', 'user')
	except:
		pass
	try:
		pw = config.get('auth', 'password')
	except:
		pass
		

# -------------------------------------------------------------------------
# override anything else we found out with the cmd line args
# -------------------------------------------------------------------------
repo = args.repo
if args.user:
	user = args.user
if args.password:
	pw = args.password

if args.public:
	is_private = False

# -------------------------------------------------------------------------
# if we still don't have values, then default the repo to cwd name and
# prompt for user/pass
# -------------------------------------------------------------------------
if not repo:
	cwd = os.path.basename(os.getcwd())
	repo = raw_input("repo name [{cwd}]: ".format(cwd=cwd))
	if not repo:
		repo = cwd

# none is different here than False
if is_private == None:
	make_public_in = raw_input("make public? [y|N] ")
	if make_public_in == "Y" or make_public_in == "y":
		is_private = False
	else:
		is_private = True

if user:
	print "user: {}  (from ~/.bbshare)".format(user)
while not user:
	user = raw_input("user: ")

while not pw:
	pw = getpass("password: ")


# -------------------------------------------------------------------------
# confirm what we want to do
# -------------------------------------------------------------------------
puborpriv = "public"
if is_private:
	puborpriv = "private"
confirm = raw_input("create repo {puborpriv} {user}/{repo} ? [Y|n] ".format(puborpriv=puborpriv, user=user, repo=repo))

# if not confirmed, bail early, otherwise continue
if confirm != "Y" and confirm != "y" and confirm != "":
	sys.exit(3)

# -------------------------------------------------------------------------
# just do it... an http post to the bitbucket api that is
# -------------------------------------------------------------------------
r = requests.post('https://api.bitbucket.org/2.0/repositories/{user}/{repo}'.format(user=user, repo=repo), auth=(user, pw), json={'scm':'git', 'is_private':is_private})
response = r.json()
if r.status_code == 200:
	# -------------------------------------------------------------------------
	# we're successufl, parse out the https clone link and display it
	# -------------------------------------------------------------------------
	clone_links = response['links']['clone']
	for cl in clone_links:
		if cl['name'] == 'https':
			clone_link = cl['href']
	if clone_link:
		print "repo created at {cl}, pushing...".format(cl=clone_link)
	else:
		print "Unable to determine clone link, but bitbucket returned ok so it's probably out there somewhere.  You'll have to fix this one yourself."
		sys.exit(4)
else:
	# -------------------------------------------------------------------------
	# we broke (probably because the repo already exists)
	# print whatever error message the api gave back
	# -------------------------------------------------------------------------
	error_message = response['error']['message']
	print "Error creating {user}/{repo}: {error}".format(user=user, repo=repo, error=error_message)
	sys.exit(5)


# -------------------------------------------------------------------------
# repo created, add it as origin and push
# -------------------------------------------------------------------------
with open(os.devnull, 'w') as f:
	rc = subprocess.call(['git', 'remote', 'add', 'origin', clone_link])
	if rc != 0:
		print "so...the repo was created but I failed to add it as the origin... help!"
		sys.exit(6)

with open(os.devnull, 'w') as f:
	rc = subprocess.call(['git', 'push', '-u', 'origin', 'master'])
	if rc != 0:
		print "almost there, but I forgot how to push to the remote repo. sorry 'bout that."
		sys.exit(7)


print ""
print "ok i shared it.  see {}".format(clone_link[:-4])