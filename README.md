# bitbucket-share-cli

No frills Python script to share a local Git repo on BitBucket.  

```
#!text
usage: bbshare.py [-h] [-u USER] [-p PASSWORD] [-r REPO]

share a local git repo on bitbucket (create remote and add as origin) default
user and password can be specified in ~/.bbconfig 

	example ~/.bbconfig
	------------------- 
	[auth] 
	user = '<user>' 
	password = '<password>'

optional arguments:
	-h, --help            show this help message and exit
  	-u USER, --user USER  your bitbucket username
  	-p PASSWORD, --password PASSWORD your bitbucket password
  	-r REPO, --repo REPO  the repo name (defaults to current dir name)
```

## requires

requests [python-requests.org](http://python-requests.org)

```
#!bash
pip install requests
```

## install

Yeah, I could figure out pip or half a dozen other packagers, but in the mean time - just do this:

```
#!bash
git clone https://slynn1324@bitbucket.org/slynn1324/bitbucket-share-cli.git
cd bitbucket-share-cli
chmod +x bbshare.py
cp bbshare.py /usr/local/bin/bbshare
```